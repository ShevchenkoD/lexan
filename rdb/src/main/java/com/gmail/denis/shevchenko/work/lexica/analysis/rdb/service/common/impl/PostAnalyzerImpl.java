package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text.Splitter;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.PostAnalyzer;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.WordStatisticService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.PostDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: ifelix
 * Date: 5/5/13
 * Time: 8:22 PM
 * To change this template use File | Settings | File Templates.
 */
@Service
public class PostAnalyzerImpl implements PostAnalyzer {

    @Autowired
    private WordStatisticService wordStatisicService;

    @Autowired
    private PostDAOService postDAOService;

    @Autowired
    private Splitter splitter;


    @Override
    public void analyzeAll() {

        List<Long> postsIDs = postDAOService.loadAllIds();

        for (Long id : postsIDs) {
            Post post = postDAOService.loadFull(id);

            if (post != null) {
                String body = post.getBody();

                if (body != null) {
                    Map<String, Integer> wordsStat = new HashMap<>();
                    Map<String, Integer> punctuatorsStat = new HashMap<>();

                    splitter.split(body, wordsStat, punctuatorsStat);


                    for (Map.Entry<String, Integer> wordEntry : wordsStat.entrySet()) {
                        wordStatisicService.updateWordCount(wordEntry.getKey(), wordEntry.getValue(), post, false);
                    }

                    for (Map.Entry<String, Integer> punctuatorEntry : punctuatorsStat.entrySet()) {
                        wordStatisicService.updateWordCount(punctuatorEntry.getKey(), punctuatorEntry.getValue(), post, true);
                    }
                }
            }

        }
    }


}
