package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;


public interface BlogDAOService extends GenericDAOService<Blog> {

    Long findBlogIdByURL(String blogURL);

    Blog loadBlogByURL(String blogURL);
}
