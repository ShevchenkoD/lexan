package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.WordStatistic;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.WordStatisticDAOService;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class WordStatisicDAOServiceImpl implements WordStatisticDAOService {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public void saveOrUpdate(WordStatistic ws) {
        sessionFactory.getCurrentSession().saveOrUpdate(ws);
    }

    @Transactional
    @Override
    public WordStatistic loadByPostAndWord(Long postId, String word) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT ws FROM WordStatistic ws JOIN ws.word w WHERE w.word = :word and ws.post.id = :postId");//
        query.setLong("postId", postId);
        query.setString("word", word);
        return (WordStatistic) query.uniqueResult();
    }
}
