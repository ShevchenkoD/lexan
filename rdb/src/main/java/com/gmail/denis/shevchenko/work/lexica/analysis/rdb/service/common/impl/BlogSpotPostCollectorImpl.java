package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Tag;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.PostCollector;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderSkipException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.utils.ASCIIUtil;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.utils.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.PostConstruct;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

@Service(value = "blogspotPostCollector")
public class BlogSpotPostCollectorImpl extends BasePostCollector implements PostCollector {

    private static final Log log = LogFactory.getLog(BlogSpotPostCollectorImpl.class);

    private XPathFactory xPathFactory = XPathFactory.newInstance();
    ;

    private XPathExpression dateExpr;
    private XPathExpression titleExpr;
    private XPathExpression bodyExpr;
    private XPathExpression bodyTagsExpr;

    @Autowired
    @Qualifier("documentProviderJSoupImpl")
    private DocumentProvider<Document> documentProvider;


    //SiteStartPage
    @PostConstruct
    public void init() {
        //  getTemplateByDomain(getDomainFromURL());


        XPath xpath = xPathFactory.newXPath();
        try {
            dateExpr = xpath.compile(".//*[@id='Blog1']/div[@class='blog-posts hfeed']/div/h2/span");
            titleExpr = xpath.compile(".//*[@id='Blog1']/div[@class=\"blog-posts hfeed\"]/div/div/div/div[@class='post hentry']/h3");
            bodyExpr = xpath.compile(".//*[@id='Blog1']/div[@class=\"blog-posts hfeed\"]/div/div/div/div[@class='post hentry']/div[starts-with(@id,'post-body')]");
            bodyTagsExpr = xpath.compile(".//*[@id='Blog1']/div[@class=\"blog-posts hfeed\"]/div/div/div/div[@class='post hentry']/div[@class='post-footer']/div[@class='post-footer-line post-footer-line-2']/span/a");

        } catch (XPathExpressionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }


    @Override
    public void collectAll(Blog blog) {

        try {
            boolean hasNext = true;
            XPath xpath = xPathFactory.newXPath();
            XPathExpression nextExpr = xpath.compile(".//*[@id='blog-pager']/span[@id='blog-pager-older-link']/a");
            XPathExpression postLinksExpr = xpath.compile(".//*[@id='Blog1']/div[@class='blog-posts hfeed']/div/div/div/div/h3/a");
            while (hasNext) {
                //(new DocumentProviderCybernekoImpl()).getDocumentFromUrl(nextLink)
                Document doc = getDocumentProvider().getDocumentFromUrl(nextLink);

                Node nextLinkNode = ((Node) nextExpr.evaluate(doc, XPathConstants.NODE));
                if (nextLinkNode != null) {
                    nextLink = nextLinkNode.getAttributes().getNamedItem("href").getNodeValue();
                } else {
                    hasNext = false;
                }
                NodeList nodes = (NodeList) (postLinksExpr).evaluate(doc, XPathConstants.NODESET);

                for (int i = 0; i < nodes.getLength(); i++) {
                    Node postNode = nodes.item(i);
                    String postURL = postNode.getAttributes().getNamedItem("href").getNodeValue();
                    saveNew(blog, postURL);
                }

            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (DocumentProviderException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (DocumentProviderSkipException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }


    protected Post parsePost(String href) {
        Post post = new Post();
        post.setUrl(href);

        try {
            Document doc = getDocumentProvider().getDocumentFromUrl(href);
      /*doc.getInputEncoding();
      doc.getXmlEncoding();  */


            post.setDateString(((Node) dateExpr.evaluate(doc, XPathConstants.NODE)).getChildNodes().item(0).getNodeValue());

            post.setTitle(((Node) titleExpr.evaluate(doc, XPathConstants.NODE)).getChildNodes().item(0).getNodeValue());

            Node bodyNode = (Node) bodyExpr.evaluate(doc, XPathConstants.NODE);
            post.setBody(processBodyNode(bodyNode, new StringBuffer()).toString());
            post.setBodyUnaccent(ASCIIUtil.convertNonAscii(post.getBody()));

            List<Tag> tags = new ArrayList<Tag>();
            NodeList nodes = (NodeList) bodyTagsExpr.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++) {
                tags.add(new Tag(nodes.item(i).getChildNodes().item(0).getNodeValue(), post));
            }
            post.setTags(tags);

        } catch (XPathExpressionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RuntimeException re) {
            log.error(MessageFormat.format("!!RUNTIME ERROR!!  URL:{0} ", href), re);
        } catch (DocumentProviderException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (DocumentProviderSkipException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

        }

        return post;
    }

    private StringBuffer processBodyNode(Node bodyNode, StringBuffer sb) {

        NodeList nodes = bodyNode.getChildNodes();
        if (nodes.getLength() > 0) {
            for (int i = 0; i < nodes.getLength(); i++) {
                sb.append(processBodyNode(nodes.item(i), new StringBuffer()));
            }
        } else {
            if ((bodyNode.getNodeType() == Node.TEXT_NODE)) {
                String nv = bodyNode.getNodeValue();
                if (nv != null) {
                    nv = new String(nv.getBytes(StringUtils.ISO_8859_1_CHARSET), StringUtils.UTF_8_CHARSET);
                    sb.append(nv);
                } else {
                    // bodyNode.getTextContent()
                }
            }
        }

        return sb;
    }

    public DocumentProvider<Document> getDocumentProvider() {
        return documentProvider;
    }

    public void setDocumentProvider(DocumentProvider<Document> documentProvider) {
        this.documentProvider = documentProvider;
    }
}
