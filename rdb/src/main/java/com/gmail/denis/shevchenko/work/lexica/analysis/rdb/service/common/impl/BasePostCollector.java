package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.PostCollector;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.SkipPostException;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.post.parser.PostParser;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.PostDAOService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;


public abstract class BasePostCollector<T> implements PostCollector<T> {
    protected String nextLink;

    private static final Log log = LogFactory.getLog(BasePostCollector.class);

    @Autowired
    private PostDAOService postDAOService;

    @Autowired
    private PostParser postParser;


    @Override
    public void setStartPage(String siteURL) {
        nextLink = siteURL;
    }

    @Transactional
    protected void saveNew(Blog blog, String postURL) {
        Long postId = postDAOService.findPostIdByURL(postURL);
        if (postId == null) {

            try {

                Post post = getPostParser().parsePost(getDocument());
                post.setBlog(blog);

                getPostDAOService().save(post);
            } catch (SkipPostException e) {
                String message = MessageFormat.format("Post is skipped URL {0}", postURL);

                log.info(message);
                System.err.println(message);

                e.printStackTrace();
            }

        }
    }


    public PostDAOService getPostDAOService() {
        return postDAOService;
    }

    @Override
    public PostParser<T> getPostParser() {
        return postParser;
    }

    @Override
    public T getDocument() {
        return null;
    }
}
