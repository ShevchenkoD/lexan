package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;



public interface WordStatisticService {

    void incrementWordCount(String word, Post post, boolean isPunctuator);

    void updateWordCount(String word, int count, Post post, boolean isPunctuator);

}
