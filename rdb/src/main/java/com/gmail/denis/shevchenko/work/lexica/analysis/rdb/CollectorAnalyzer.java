package com.gmail.denis.shevchenko.work.lexica.analysis.rdb;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.BlogService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.PostAnalyzer;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.PostCollector;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.SiteService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider.BlogProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CollectorAnalyzer {


    public static final String HTTP_ES_WIKIPEDIA_ORG = "http://es.wikipedia.org";
    private static final String HTTP_BLOGSPOT_COM = "http://blogspot.com/";

    /**
     * @param args
     */
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        BlogProvider blogProvider = context.getBean("blogProviderGoogleJsoup", BlogProvider.class);

        BlogProvider wikiBlogProvider = context.getBean("wikiBlogProvider", BlogProvider.class);

        PostCollector blogspotPostCollector = context.getBean("blogSpotPostCollectorJSoup", PostCollector.class);//blogspotPostCollector

        PostCollector wikiPostCollector = context.getBean("wikiPostCollectorJSoup", PostCollector.class);

        PostAnalyzer postAnalyzer = context.getBean(PostAnalyzer.class);

        BlogService blogService = context.getBean(BlogService.class);

        SiteService siteService = context.getBean(SiteService.class);

        Site wikiSite = initWiki(siteService);
        Site blogspotSite = initBlogSpot(siteService);

        collectPages(wikiBlogProvider, wikiPostCollector, blogService, wikiSite);
        collectPages(blogProvider, blogspotPostCollector, blogService, blogspotSite);

        postAnalyzer.analyzeAll();

    }

    private static Site initBlogSpot(SiteService siteService) {
        Site site = siteService.loadSiteByURL(HTTP_BLOGSPOT_COM);
        if (site == null) {
            site = new Site(HTTP_BLOGSPOT_COM);
            siteService.save(site);
        }

        return site;
    }

    private static Site initWiki(SiteService siteService) {
        Site site = siteService.loadSiteByURL(HTTP_ES_WIKIPEDIA_ORG);
        if (site == null) {
            site = new Site(HTTP_ES_WIKIPEDIA_ORG);
            siteService.save(site);
        }

        return site;
    }


    private static void collectPages(BlogProvider blogProvider, PostCollector postCollector, BlogService blogService, Site site) {

        while (blogProvider.hasNext()) {
            String urlString = blogProvider.getNextBlogUrlString(site);
            Blog blog = blogService.loadBlogByURL(urlString);
            if (blog == null) {
                blog = new Blog(urlString, site);
                blogService.save(blog);
            }

            postCollector.setStartPage(urlString);
            postCollector.collectAll(blog);
        }
    }

}
