package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider.BlogProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderSkipException;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.BlogDAOService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service(value = "blogProviderGoogleJsoup")
public class BlogProviderGoogleJSoupImpl implements BlogProvider {

    private static final Log log = LogFactory.getLog(BlogProviderGoogleJSoupImpl.class);
    private static final String GOOGLE = "http://www.google.com";

    protected String nextLink = "http://www.google.com/search?lr=lang_es&as_qdr=all&as_sitesearch=blogspot.com&as_occt=any&safe=images";

    @Autowired
    private BlogDAOService blogDAOService;

    @Autowired
    @Qualifier("documentProviderJSoupImpl")
    private DocumentProvider<Document> documentProvider;

    private boolean hasNext = true;
    private boolean nodesIsEmpty = true;

    private int index = 0;
    private Elements nextLinkElements;


    @Override
    public String getNextBlogUrlString(Site site) {
        //
        if ((nextLinkElements == null) || (index == (nextLinkElements.size() - 1))) {
            Document doc = null;
            try {

                doc = documentProvider.getDocumentFromUrl(nextLink);

            } catch (DocumentProviderException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                throw new RuntimeException(e);
            } catch (DocumentProviderSkipException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                throw new RuntimeException(e);
            }

            if (doc != null) {


                Element nextLinkEl = doc.select(".b>a").first();
                if (nextLinkEl != null) {
                    nextLink = GOOGLE + nextLinkEl.attr("href");
                } else {
                    hasNext = false;
                }

                nextLinkElements = doc.select(".kv>cite");

                if (nextLinkElements.size() == 0) {
                    hasNext = false;
                    throw new RuntimeException("No nextLinkElements.");
                }
                index = 0;

            }


        } else {
            index++;
        }

        return "http://" + nextLinkElements.get(index).text();
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }
}
