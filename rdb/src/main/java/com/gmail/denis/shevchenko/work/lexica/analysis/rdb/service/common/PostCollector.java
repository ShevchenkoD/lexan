package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.post.parser.PostParser;

public interface PostCollector<T> {

    void init();

    void setStartPage(String siteURL);

    void collectAll(Blog blog);

    public PostParser<T> getPostParser();

    public T getDocument();
}
