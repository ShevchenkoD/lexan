package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Tag;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.PostCollector;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderSkipException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.utils.ASCIIUtil;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.utils.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.annotation.PostConstruct;
import javax.xml.xpath.*;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

@Service(value = "wikiPostCollector")
public class WikiPostCollectorImpl extends BasePostCollector implements PostCollector {

    private static final Log log = LogFactory.getLog(WikiPostCollectorImpl.class);

    @Autowired //@Qualifier("cybernekoDocumentProvider")
    private DocumentProvider<Document> documentProvider;

    private XPathFactory xPathFactory = XPathFactory.newInstance();
    private XPath xpath = xPathFactory.newXPath();
    private XPathExpression dateExpr;
    private XPathExpression titleExpr;
    private XPathExpression bodyExpr;
    private XPathExpression bodyTagsExpr;
    private XPathExpression postLinksExpr;

    {

        try {
            dateExpr = xpath.compile(".//*[@id='footer-info-lastmod']");
            titleExpr = xpath.compile(".//*[@id='firstHeading']/span");
            bodyExpr = xpath.compile(".//*[@id='mw-content-text']");
            bodyTagsExpr = xpath.compile(".//*[@id='mw-normal-catlinks']/ul/li/a");
            postLinksExpr = xpath.compile(".//*[@id='mw-content-text']/table[3]/tr[3]/td/p/a");

        } catch (XPathExpressionException e) {
            log.error(e);
        }
    }


    //SiteStartPage
    @PostConstruct
    public void init() {
        //  getTemplateByDomain(getDomainFromURL());


    }


    @Override
    public void collectAll(Blog blog) {

        try {
            boolean hasNext = true;
            XPath xpath = xPathFactory.newXPath();


            //XPathExpression nextExpr = xpath.compile(".//*[@id='mw-content-text']/div/table[2]/tbody/tr[3]/td/p/a");

            while (hasNext) {

                Document doc = getDocumentProvider().getDocumentFromUrl(nextLink);

                NodeList nodes = (NodeList) postLinksExpr.evaluate(doc, XPathConstants.NODESET);

                for (int i = 0; i < nodes.getLength(); i++) {
                    Node postNode = nodes.item(i);
                    String postURL = postNode.getAttributes().getNamedItem("href").getNodeValue();
                    postURL = blog.getSite().getUrl() + postURL;
                    saveNew(blog, postURL);
                }

                hasNext = false;
            }
        } catch (XPathExpressionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (DocumentProviderException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (DocumentProviderSkipException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }


    protected Post parsePost(String href) {
        Post post = new Post();
        post.setUrl(href);

        try {
            Document doc = getDocumentProvider().getDocumentFromUrl(href);


            post.setDateString(((Node) dateExpr.evaluate(doc, XPathConstants.NODE)).getChildNodes().item(0).getNodeValue());

            post.setTitle(((Node) titleExpr.evaluate(doc, XPathConstants.NODE)).getChildNodes().item(0).getNodeValue());

            Node bodyNode = (Node) bodyExpr.evaluate(doc, XPathConstants.NODE);

            post.setBody(processBodyNode(bodyNode, new StringBuffer()).toString());

            post.setBodyUnaccent(ASCIIUtil.convertNonAscii(post.getBody()));

            List<Tag> tags = new ArrayList<Tag>();
            NodeList nodes = (NodeList) bodyTagsExpr.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++) {
                tags.add(new Tag(nodes.item(i).getChildNodes().item(0).getNodeValue(), post));
            }

            post.setTags(tags);
        } catch (XPathExpressionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (RuntimeException re) {
            re.printStackTrace();
            log.error(MessageFormat.format("!!!RUNTIME ERROR!!! URL {0} ", href), re);
            throw re;
        } catch (DocumentProviderException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (DocumentProviderSkipException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return post;
    }

    private StringBuffer processBodyNode(Node bodyNode, StringBuffer sb) {

        NodeList nodes = bodyNode.getChildNodes();
        if (nodes.getLength() > 0) {
            for (int i = 0; i < nodes.getLength(); i++) {
                sb.append(processBodyNode(nodes.item(i), new StringBuffer()));
            }
        } else {
            if ((bodyNode.getNodeType() == Node.TEXT_NODE)) {
                String nv = bodyNode.getNodeValue();
                if (nv != null) {
                    nv = new String(nv.getBytes(StringUtils.ISO_8859_1_CHARSET), StringUtils.UTF_8_CHARSET);
                    sb.append(nv);
                } else {
                    // bodyNode.getTextContent()
                }
            }

        }

        return sb;
    }

    public DocumentProvider<Document> getDocumentProvider() {
        return documentProvider;
    }

    public void setDocumentProvider(DocumentProvider<Document> documentProvider) {
        this.documentProvider = documentProvider;
    }
}
