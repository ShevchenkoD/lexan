package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.post.parser;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.SkipPostException;

public interface PostParser<T> {
    Post parsePost(T document) throws SkipPostException;
}
