package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Word;


public interface WordDAOService extends GenericDAOService<Word> {

    Word findByWord(String word);

}
