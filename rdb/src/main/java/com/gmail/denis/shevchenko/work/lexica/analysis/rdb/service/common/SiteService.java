package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.SiteDAOService;


public interface SiteService extends WAService<Site, SiteDAOService> {
    Site loadSiteByURL(String url);
}
