package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Blog",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"url"}
        ))
public class Blog implements Serializable {

    private static final long serialVersionUID = -5600690844127238269L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Column(name = "URL")
    private String url;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "blog")
    @Cascade(value = {org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Post> posts;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "SITE_ID", nullable = false)
    private Site site;

    public Blog() {
    }

    public Blog(String urlString, Site site) {
        this.url = urlString;
        this.site = site;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public Site getSite() {
        return site;
    }

    public void setSite(Site site) {
        this.site = site;
    }
}
