package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Tag;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.PostCollector;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderSkipException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.SkipPostException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.utils.ASCIIUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: ifelix
 * Date: 9/22/13
 * Time: 1:48 AM
 * To change this template use File | Settings | File Templates.
 */
@Service(value = "blogSpotPostCollectorJSoup")
public class BlogSpotPostCollectorJSoupImpl extends BasePostCollector implements PostCollector {

    private static final Log log = LogFactory.getLog(BlogSpotPostCollectorImpl.class);

    @Autowired
    @Qualifier("documentProviderJSoupImpl")
    private DocumentProvider<Document> documentProvider;


    @Override
    public void init() {

    }

    @Override
    public void collectAll(Blog blog) {
        try {
            boolean hasNext = true;
            while (hasNext) {

                Document doc = getDocumentProvider().getDocumentFromUrl(nextLink);

                Element nextLinkElement = doc.select("#blog-pager span#blog-pager-older-link > a").first();

                if (nextLinkElement != null) {
                    nextLink = nextLinkElement.attr("href");
                } else {
                    hasNext = false;
                }

                Elements postAnchorElements = doc.select(".post-title.entry-title>a");

                //ArrayList<Post> posts = new ArrayList<Post>();

                for (Element postAnchorElement : postAnchorElements) {
                    String postURL = postAnchorElement.attr("href");
                    saveNew(blog, postURL);
                }

            }
        } catch (DocumentProviderException e) {
            throw new RuntimeException(e);
        } catch (DocumentProviderSkipException e) {
            log.error("Blog is skipped", e);
        }

    }


    protected Post parsePost(String href) throws SkipPostException {
        Post post = new Post();
        post.setUrl(href);
        try {
            Document doc = getDocumentProvider().getDocumentFromUrl(href);

            Element dateElement = doc.select(".date-header>span").first();
            if (dateElement != null) {
                post.setDateString(dateElement.text());
            }

            Element titleElement = doc.select(".post-title.entry-title").first();
            if (titleElement != null) {
                post.setTitle(titleElement.text());
            }

            Elements bodyElement = doc.select(".post-body.entry-content");
            if (bodyElement.first() != null) {
                post.setBody(processBodyElements(bodyElement, new StringBuffer()).toString());
                post.setBodyUnaccent(ASCIIUtil.convertNonAscii(post.getBody()));

            }

            Elements tagElements = doc.select(".post-labels a");
            if (tagElements.first() != null) {
                // throw new SkipPostException("");
                List<Tag> tags = new ArrayList<Tag>();
                for (Element tagElement : tagElements) {
                    tags.add(new Tag(tagElement.text(), post));
                }

                post.setTags(tags);
            }

        } catch (RuntimeException re) {
            re.printStackTrace();
            String message = MessageFormat.format("!!!RUNTIME ERROR!!! URL {0} ", href);
            System.err.println(message);
            log.error(message, re);
            throw re;
        } catch (DocumentProviderException e) {
            e.printStackTrace();
            String message = MessageFormat.format("Document parser error. URL {0} ", href);
            System.err.println(message);
            log.error(message, e);
            throw new RuntimeException(e);

        } catch (DocumentProviderSkipException e) {
            e.printStackTrace();
            String message = MessageFormat.format("Document is skipped. URL {0} ", href);
            System.err.println(message);
            log.error(message, e);
        }

        return post;
    }

    private StringBuffer processBodyElements(Elements els, StringBuffer sb) {
        for (Element el : els) {
            processBodyElement(el, sb);
        }

        return sb;
    }

    private void processBodyElement(Element el, StringBuffer sb) {
        if (!el.text().isEmpty()) {
            sb.append(el.text());
        }
        /*
        List<TextNode> textNodes = el.textNodes();
        if ((textNodes != null) && textNodes.size() > 0) {

            for (TextNode textNode : textNodes){
                sb.append(el.text());
            }
        }

        Elements children = el.children();
        if ((children != null) && children.size() > 0) {

            for (Element child : children){
                processBodyElement(child, sb);
            }
        }*/
    }


    public DocumentProvider<Document> getDocumentProvider() {
        return documentProvider;
    }

    public void setDocumentProvider(DocumentProvider<Document> documentProvider) {
        this.documentProvider = documentProvider;
    }
}
