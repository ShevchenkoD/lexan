package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider.BlogProvider;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service(value = "wikiBlogProvider")
public class BlogProviderWikiImpl implements BlogProvider {
    private List<String> siteURLs = new ArrayList<>();

    {
        siteURLs.add("http://es.wikipedia.org/wiki/Wikipedia:Art%C3%ADculos_destacados");
    }

    private Iterator<String> iterator = siteURLs.iterator();

    @Override
    public String getNextBlogUrlString(Site site) {
        return iterator.next();
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }
}
