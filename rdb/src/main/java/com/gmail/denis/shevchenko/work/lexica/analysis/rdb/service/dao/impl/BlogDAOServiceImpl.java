package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.BlogDAOService;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class BlogDAOServiceImpl extends BaseDAOService<Blog> implements BlogDAOService {

    @Transactional
    @Override
    public Long findBlogIdByURL(String blogURL) {
        Session session = getSessionFactory().getCurrentSession();
        Query query = session.createQuery("SELECT b.id FROM Blog b WHERE b.url = ?");
        query.setString(0, blogURL);
        return (Long) query.uniqueResult();
    }

    @Transactional
    @Override
    public Blog loadBlogByURL(String blogURL) {
        Session session = getSessionFactory().getCurrentSession();
        Query query = session.createQuery("SELECT b FROM Blog b WHERE b.url = ?");
        query.setString(0, blogURL);
        return (Blog) query.uniqueResult();
    }

    @Override
    public Class<Blog> getClazz() {
        return Blog.class;
    }
}
