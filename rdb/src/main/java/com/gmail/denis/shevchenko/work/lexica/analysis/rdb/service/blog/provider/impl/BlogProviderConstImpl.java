package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider.BlogProvider;
import org.springframework.stereotype.Service;

@Service
public class BlogProviderConstImpl implements BlogProvider {
    private List<String> siteURLs = new ArrayList<>();

    {
        siteURLs.add("http://ponunamargaritaentusombrero.blogspot.com");
        siteURLs.add("http://suenosycaramelosstyle.blogspot.com/");
        siteURLs.add("http://penyalestepec.blogspot.com");
    }

    private Iterator<String> iterator = siteURLs.iterator();

    @Override
    public String getNextBlogUrlString(Site site) {
        return iterator.next();
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }
}
