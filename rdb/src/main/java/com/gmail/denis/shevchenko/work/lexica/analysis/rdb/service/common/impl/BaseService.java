package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.WAService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.GenericDAOService;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

public abstract class BaseService<T, D extends GenericDAOService<T>> implements WAService<T, D> {

    @Transactional
    @Override
    public T loadById(Serializable id) {
        return getDaoService().load(id);
    }

    @Override
    public Serializable save(T entity) {
        return getDaoService().save(entity);
    }


}
