package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;

import java.util.List;


public interface PostDAOService extends GenericDAOService<Post> {

    List<Long> loadAllIds();

    List<Post> loadAll();

    Long findPostIdByURL(String postURL);

    Post loadFull(Long id);
}
