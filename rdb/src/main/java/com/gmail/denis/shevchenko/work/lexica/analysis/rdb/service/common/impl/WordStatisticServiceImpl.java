package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Word;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.WordStatistic;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.WordUnaccent;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.WordStatisticService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.PostDAOService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.WordDAOService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.WordStatisticDAOService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.WordUnaccentDAOService;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.utils.ASCIIUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class WordStatisticServiceImpl implements WordStatisticService {
    @Autowired
    private PostDAOService postDAOService;

    @Autowired
    private WordStatisticDAOService wordStatisticDAOService;

    @Autowired
    private WordDAOService wordDAOService;

    @Autowired
    private WordUnaccentDAOService wordUnaccentDAOService;

    //@Transactional
    @Override
    public void incrementWordCount(String word, Post post, boolean isPunctuator) {

        WordStatistic ws = wordStatisticDAOService.loadByPostAndWord(post.getId(), word);

        WordUnaccent unaccentWordEntity = null;
        if (ws == null) {
            if (!isPunctuator) {

                String unaccentWord = ASCIIUtil.convertNonAscii(word);
                unaccentWordEntity = wordUnaccentDAOService.findByWord(unaccentWord);

                if (unaccentWordEntity == null) {
                    unaccentWordEntity = new WordUnaccent(unaccentWord);
                }
            }


            Word wordEntity = wordDAOService.findByWord(word);

            if (wordEntity == null) {
                wordEntity = new Word(word);
                wordEntity.setPunctuator(isPunctuator);
                wordEntity.setWordUnaccent(unaccentWordEntity);
            }

            ws = new WordStatistic();
            ws.setCount(1);
            ws.setPost(post);
            ws.setWord(wordEntity);

            wordStatisticDAOService.saveOrUpdate(ws);
        } else {
            ws.setCount(ws.getCount() + 1);
        }

        wordStatisticDAOService.saveOrUpdate(ws);
    }


    @Override
    public void updateWordCount(String word, int count, Post post, boolean isPunctuator) {
        WordStatistic ws = wordStatisticDAOService.loadByPostAndWord(post.getId(), word);

        WordUnaccent unaccentWordEntity = null;
        if (ws == null) {
            if (!isPunctuator) {

                String unaccentWord = ASCIIUtil.convertNonAscii(word);
                unaccentWordEntity = wordUnaccentDAOService.findByWord(unaccentWord);

                if (unaccentWordEntity == null) {
                    unaccentWordEntity = new WordUnaccent(unaccentWord);
                }
            }


            Word wordEntity = wordDAOService.findByWord(word);

            if (wordEntity == null) {
                wordEntity = new Word(word);
                wordEntity.setPunctuator(isPunctuator);
                wordEntity.setWordUnaccent(unaccentWordEntity);
            }

            ws = new WordStatistic();
            ws.setCount(count);
            ws.setPost(post);
            ws.setWord(wordEntity);

            wordStatisticDAOService.saveOrUpdate(ws);
        } else {
            ws.setCount(count);
        }

        wordStatisticDAOService.saveOrUpdate(ws);

    }

    private int findStatisticIndex(List<WordStatistic> wsl, String word) {
        boolean found = false;
        int i;
        for (i = 0; (i < wsl.size()) && (!found); i++) {
            WordStatistic ws = wsl.get(i);
            if (ws.getWord().getWord().equals(word)) {
                found = true;
            }
        }

        return found ? i - 1 : -1;
    }


}
