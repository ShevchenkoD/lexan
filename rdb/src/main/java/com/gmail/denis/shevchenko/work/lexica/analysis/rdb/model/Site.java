package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Site",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"url"}
        ))
public class Site implements Serializable {

    private static final long serialVersionUID = -2796163581174125999L;


    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long id;


    @Column(name = "URL")
    private String url;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "site")
    @Cascade(value = {org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Blog> blogs;

    public Site() {
    }

    public Site(String urlString) {
        this.url = urlString;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public List<Blog> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<Blog> blogs) {
        this.blogs = blogs;
    }
}
