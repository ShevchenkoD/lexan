package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.GenericDAOService;
import org.hibernate.Hibernate;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;


public abstract class BaseDAOService<T> implements GenericDAOService<T> {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public Serializable save(T entity) {

        Serializable id = getSessionFactory().getCurrentSession().save(entity);
        getSessionFactory().getCurrentSession().flush();

        return id;
    }

    @Transactional
    @Override
    public void saveOrUpdate(T entity) {
        getSessionFactory().getCurrentSession().saveOrUpdate(entity);
        getSessionFactory().getCurrentSession().flush();
    }

    @Transactional
    @Override
    public T load(Serializable id) {

        T result = (T) getSessionFactory().getCurrentSession().load(getClazz(), id);
        Hibernate.initialize(result);
        return result;
    }


    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
