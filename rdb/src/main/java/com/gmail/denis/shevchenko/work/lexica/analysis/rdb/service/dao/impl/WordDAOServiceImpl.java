package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Word;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.WordDAOService;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;


@Repository
public class WordDAOServiceImpl extends BaseDAOService<Word> implements WordDAOService {


    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Serializable save(Word word) {
        return sessionFactory.getCurrentSession().save(word);
    }

    @Override
    public void saveOrUpdate(Word word) {
        sessionFactory.getCurrentSession().saveOrUpdate(word);
    }

    @Transactional
    @Override
    public Word findByWord(String word) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT w FROM Word w WHERE w.word=?");
        query.setString(0, word);
        return (Word) query.uniqueResult();
    }

    @Override
    public Class<Word> getClazz() {
        return Word.class;
    }
}
