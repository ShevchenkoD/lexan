package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider.BlogProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderSkipException;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.BlogDAOService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.xpath.*;


@Service(value = "blogProvider")
public class BlogProviderGoogleImpl implements BlogProvider {
    private static final Log log = LogFactory.getLog(BlogProviderGoogleImpl.class);

    private XPathFactory xPathFactory = XPathFactory.newInstance();
    private XPath xpath = xPathFactory.newXPath();
    XPathExpression nextExpr;
    XPathExpression blogLinksExpr;

    {
        try {
            blogLinksExpr = xpath.compile(".//*[@id='rso']/LI/DIV/DIV/DIV/DIV/CITE");
            nextExpr = xpath.compile(".//*[@id='pnnext']");
        } catch (XPathExpressionException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    protected String nextLink = "http://www.google.com/search?lr=lang_es&as_qdr=all&as_sitesearch=blogspot.com&as_occt=any&safe=images";

    @Autowired
    private BlogDAOService blogDAOService;

    @Autowired
    @Qualifier("cybernekoDocumentProvider")
    private DocumentProvider<Document> documentProvider;

    private boolean hasNext = true;
    private boolean nodesIsEmpty = true;
    private NodeList nodes;
    private int index = 0;


    @Override
    public String getNextBlogUrlString(Site site) {
        //
        if ((nodes == null) || (index == (nodes.getLength()))) {
            Document doc = null;
            try {

                doc = documentProvider.getDocumentFromUrl(nextLink);

            } catch (DocumentProviderException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                log.error(e);
            } catch (DocumentProviderSkipException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                log.error(e);
            }

            if (doc != null) {
                try {
                    Node nextLinkNode = ((Node) nextExpr.evaluate(doc, XPathConstants.NODE));
                    if (nextLinkNode != null) {
                        nextLink = nextLinkNode.getAttributes().getNamedItem("href").getNodeValue();
                    } else {
                        hasNext = false;
                    }

                    nodes = (NodeList) (blogLinksExpr).evaluate(doc, XPathConstants.NODESET);
                    if (nodes.getLength() == 0) {
                        hasNext = false;
                        throw new RuntimeException("No nodes.");
                    }
                    index = 0;
                } catch (XPathExpressionException ex) {
                    log.debug(ex);
                    throw new RuntimeException(ex);
                }

            }
            //(xpath.compile(".//*[@id='rso']/li/div/div/div/div/cite")).evaluate(doc, XPathConstants.NODE);


        } else {
            index++;
        }

        return "http://" + nodes.item(index).getTextContent();
    }

    @Override
    public boolean hasNext() {
        return hasNext;
    }
}
