package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.WordStatistic;


public interface WordStatisticDAOService {

    //void updateWordCount(String word, Integer postId);

    void saveOrUpdate(WordStatistic ws);

    WordStatistic loadByPostAndWord(Long postId, String word);
}
