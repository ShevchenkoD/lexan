package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;


public interface SiteDAOService extends GenericDAOService<Site> {

    @Override
    @Transactional
    Serializable save(Site site);

    @Transactional
    Site loadSiteByURL(String siteURL);

}
