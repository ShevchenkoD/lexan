package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.blog.provider;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;

public interface BlogProvider {

    boolean hasNext();

    String getNextBlogUrlString(Site site);
}
