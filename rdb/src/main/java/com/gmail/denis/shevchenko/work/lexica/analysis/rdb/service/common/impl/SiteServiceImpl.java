package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.SiteService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.SiteDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SiteServiceImpl extends BaseService<Site, SiteDAOService> implements SiteService {

    @Autowired
    private SiteDAOService siteDAOService;

    @Override
    public Site loadSiteByURL(String url) {
        return getDaoService().loadSiteByURL(url);
    }

    @Override
    public SiteDAOService getDaoService() {
        return siteDAOService;
    }
}
