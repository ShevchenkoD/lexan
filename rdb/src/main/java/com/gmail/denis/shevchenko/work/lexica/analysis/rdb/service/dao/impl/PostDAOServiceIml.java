package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.PostDAOService;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
public class PostDAOServiceIml extends BaseDAOService<Post> implements PostDAOService {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public List<Long> loadAllIds() {
        Query query = sessionFactory.getCurrentSession().createQuery("Select p.id from Post p");
        return query.list();
    }

    @Transactional
    @Override
    public List<Post> loadAll() {
        Query query = sessionFactory.getCurrentSession().createQuery("from Post");
        return query.list();
    }

    @Transactional
    @Override
    public Post loadFull(Long id) {
        return load(id);
    }

    @Override
    public Class<Post> getClazz() {
        return Post.class;
    }


    @Transactional
    @Override
    public Long findPostIdByURL(String postURL) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("SELECT p.id FROM Post p WHERE p.url = ?");
        query.setString(0, postURL);
        return (Long) query.uniqueResult();
    }
}
