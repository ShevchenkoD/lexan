package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "WORD",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"word"}
        )
)
public class Word implements Serializable {
    private static final long serialVersionUID = -1922196422043251896L;

    @Id
    @GeneratedValue
    @Column(name = "ID", unique = true, nullable = false)
    Long id;


    @Column(name = "WORD", unique = true, nullable = false)
    String word;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "WORD_UNNACENT_ID", nullable = true)
    WordUnaccent wordUnaccent;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "word", cascade = {CascadeType.ALL})
    private List<WordStatistic> wordStatistic;

    @Column(name = "PUNCTUATOR", nullable = false)
    private boolean isPunctuator = false;


  /*
  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "PARENT_ID", nullable = true)
  Word parent;  */

    public Word() {
        this.isPunctuator = false;
    }

    public Word(String word) {
        this.word = word;
        this.isPunctuator = false;
    }

    public List<WordStatistic> getWordStatistic() {
        return wordStatistic;
    }

    public void setWordStatistic(List<WordStatistic> wordStatistic) {
        this.wordStatistic = wordStatistic;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Word)) return false;

        Word word1 = (Word) o;

        if (!word.equals(word1.word)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }


    public WordUnaccent getWordUnaccent() {
        return wordUnaccent;
    }

    public void setWordUnaccent(WordUnaccent wordUnaccent) {
        this.wordUnaccent = wordUnaccent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isPunctuator() {
        return isPunctuator;
    }

    public void setPunctuator(boolean punctuator) {
        isPunctuator = punctuator;
    }
}
