package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.PostCollector;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderSkipException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service(value = "wikiPostCollectorJSoup")
public class WikiPostCollectorJSoupImpl extends BasePostCollector implements PostCollector {

    private static final Log log = LogFactory.getLog(WikiPostCollectorJSoupImpl.class);

    @Autowired
    @Qualifier("documentProviderJSoupImpl")
    private DocumentProvider<Document> documentProvider;


    //SiteStartPage
    @PostConstruct
    public void init() {
    }


    @Override
    public void collectAll(Blog blog) {

        try {
            boolean hasNext = true;

            while (hasNext) {

                Document doc = getDocumentProvider().getDocumentFromUrl(nextLink);
                Elements nodeElements = doc.select("#mw-content-text>table:eq(2)>tbody>tr:eq(2)>td>p>a");

                for (Element postNode : nodeElements) {
                    String postURL = postNode.attr("href");
                    postURL = blog.getSite().getUrl() + postURL;
                    saveNew(blog, postURL);
                }

                hasNext = false;
            }
        } catch (DocumentProviderException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } catch (DocumentProviderSkipException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

    }


    public DocumentProvider<Document> getDocumentProvider() {
        return documentProvider;
    }

    public void setDocumentProvider(DocumentProvider<Document> documentProvider) {
        this.documentProvider = documentProvider;
    }
}
