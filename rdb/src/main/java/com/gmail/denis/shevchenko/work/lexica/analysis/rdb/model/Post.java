package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model;

import org.hibernate.annotations.Cascade;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "POST",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"url"}
        ))
public class Post implements Serializable {

    private static final long serialVersionUID = -5600690844127238269L;

    @Id
    @Column(name = "ID")
    @GeneratedValue
    private Long id;

    @Column(name = "DATE_STRING")
    private String dateString;

    @Column(name = "TITLE")
    private String title;

    @Column(name = "URL")
    private String url;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post")
    @Cascade(value = {org.hibernate.annotations.CascadeType.SAVE_UPDATE})
    private List<Tag> tags;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "BLOG_ID", nullable = false)
    private Blog blog;

    @Column(name = "BODY")
    @Lob
    private String body;


    @Column(name = "BODY_UNACCENT")
    @Lob
    private String bodyUnaccent;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post", cascade = {CascadeType.ALL})
    //@Cascade(value = { org.hibernate.annotations.CascadeType.SAVE_UPDATE })
    private List<WordStatistic> wordStatistic;

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getDateString() {
        return dateString;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<WordStatistic> getWordStatistic() {
        return wordStatistic;
    }

    public void setWordStatistic(List<WordStatistic> wordStatistic) {
        this.wordStatistic = wordStatistic;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Blog getBlog() {
        return blog;
    }

    public void setBlog(Blog blog) {
        this.blog = blog;
    }

    public String getBodyUnaccent() {
        return bodyUnaccent;
    }

    public void setBodyUnaccent(String bodyUnaccent) {
        this.bodyUnaccent = bodyUnaccent;
    }
}
