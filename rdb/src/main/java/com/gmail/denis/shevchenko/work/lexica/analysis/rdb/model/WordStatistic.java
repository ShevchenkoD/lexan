package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "WORD_STATISTIC")
public class WordStatistic implements Serializable {
    private static final long serialVersionUID = -1922196422043251896L;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "POST_ID", nullable = false)
    private Post post;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "WORD_ID", nullable = false)
    private Word word;

    @Column(name = "COUNT")
    private Integer count;

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
