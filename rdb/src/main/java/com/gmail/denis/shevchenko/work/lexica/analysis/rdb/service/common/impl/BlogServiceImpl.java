package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common.BlogService;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.BlogDAOService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class BlogServiceImpl extends BaseService<Blog, BlogDAOService> implements BlogService {
    @Autowired
    BlogDAOService blogDAOService;

    @Transactional
    @Override
    public Long save(Blog blog) {

        return (Long) getDaoService().save(blog);
    }

    @Override
    public Blog loadBlogByURL(String url) {
        return getDaoService().loadBlogByURL(url);
    }

    @Override
    public BlogDAOService getDaoService() {
        return blogDAOService;
    }
}
