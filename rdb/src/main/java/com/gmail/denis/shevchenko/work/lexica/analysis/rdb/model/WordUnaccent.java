package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;


@Entity
@Table(name = "WORD_UNACCENT",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"word"}
        )
)
public class WordUnaccent implements Serializable {
    private static final long serialVersionUID = -1922196422043251896L;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue
    Long id;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "wordUnaccent", cascade = {CascadeType.ALL})
    private List<Word> words;

    @Column(name = "WORD", unique = true, nullable = false)
    String word;

    public WordUnaccent() {
    }

    public WordUnaccent(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Word> getWords() {
        return words;
    }

    public void setWords(List<Word> words) {
        this.words = words;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WordUnaccent)) return false;

        WordUnaccent word1 = (WordUnaccent) o;

        if (!word.equals(word1.word)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }

}
