package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao;

import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;


public interface GenericDAOService<T> {

    Serializable save(T entity);

    T load(Serializable id);

    public Class<T> getClazz();

    @Transactional
    void saveOrUpdate(T entity);
}
