package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common;

import java.io.Serializable;


public interface WAService<T, D> {

    public T loadById(Serializable id);

    public Serializable save(T entity);

    public D getDaoService();
}
