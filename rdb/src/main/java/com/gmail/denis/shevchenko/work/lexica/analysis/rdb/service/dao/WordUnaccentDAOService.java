package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.WordUnaccent;


public interface WordUnaccentDAOService extends GenericDAOService<WordUnaccent> {
    WordUnaccent findByWord(String word);
}
