package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.common;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Blog;


public interface BlogService {
    Long save(Blog blog);

    Blog loadBlogByURL(String url);
}
