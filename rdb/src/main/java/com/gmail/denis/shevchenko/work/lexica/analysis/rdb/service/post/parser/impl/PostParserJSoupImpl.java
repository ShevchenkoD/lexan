package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.post.parser.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Post;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Tag;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.post.parser.PostParser;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.utils.ASCIIUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


public class PostParserJSoupImpl implements PostParser<Document> {

    private static final Log log = LogFactory.getLog(PostParserJSoupImpl.class);


    @Autowired
    @Qualifier("documentProviderJSoupImpl")
    private DocumentProvider<Document> documentProvider;

    @Override
    public Post parsePost(Document doc) {
        Post post = new Post();
        String href = "";// doc.absUrl()
        post.setUrl(href);

        try {

            //Document doc = getDocumentProvider().getDocumentFromUrl(href);

            //
            post.setDateString(doc.select("#footer-info-lastmod").first().text());

            post.setTitle(doc.select("#firstHeading>span").first().text());

            post.setBody(doc.select("#mw-content-text").text());

            post.setBodyUnaccent(ASCIIUtil.convertNonAscii(post.getBody()));

            List<Tag> tags = new ArrayList<Tag>();
            Elements nodeElements = doc.select("#mw-normal-catlinks>ul>li>a");
            for (int i = 0; i < nodeElements.size(); i++) {
                tags.add(new Tag(nodeElements.get(i).text(), post));
            }

            post.setTags(tags);
        } catch (RuntimeException re) {
            re.printStackTrace();
            log.error(MessageFormat.format("!!!RUNTIME ERROR!!! URL {0} ", href), re);
            throw re;
        }

        return post;
    }

    public DocumentProvider<Document> getDocumentProvider() {
        return documentProvider;
    }


}
