package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.WordUnaccent;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.WordUnaccentDAOService;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class WordUnaccentDAOServiceImpl extends BaseDAOService<WordUnaccent> implements WordUnaccentDAOService {

    @Transactional
    @Override
    public WordUnaccent findByWord(String word) {
        Session session = getSessionFactory().getCurrentSession();
        Query query = session.createQuery("SELECT w FROM WordUnaccent w WHERE w.word=?");
        query.setString(0, word);
        return (WordUnaccent) query.uniqueResult();
    }


    @Override
    public Class<WordUnaccent> getClazz() {
        return WordUnaccent.class;
    }
}
