package com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.model.Site;
import com.gmail.denis.shevchenko.work.lexica.analysis.rdb.service.dao.SiteDAOService;
import org.hibernate.Query;
import org.hibernate.classic.Session;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class SiteDAOServiceImpl extends BaseDAOService<Site> implements SiteDAOService {

    @Transactional
    @Override
    public Site loadSiteByURL(String siteURL) {
        Session session = getSessionFactory().getCurrentSession();
        Query query = session.createQuery("SELECT s FROM Site s WHERE s.url = ?");
        query.setString(0, siteURL);
        return (Site) query.uniqueResult();
    }

    @Override
    public Class<Site> getClazz() {
        return Site.class;
    }
}
