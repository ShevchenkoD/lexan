package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.utils.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ifelix on 2/22/15.
 */
public class SplitterTest {

    Splitter splitter = new SplitterImpl();


    @Test
    public void testSplitToMap (){
        String text = "tata, tutu!!! olo lom. !!!qe??? pasas;";

        Map<String, Integer> words = new HashMap<>();
        Map<String, Integer> punctuators = new HashMap<>();
        splitter.split(text, words, punctuators);

        Assert.assertEquals(6, words.size());
        Assert.assertEquals(6, punctuators.size());

    }


    @Test
    public void testSplitToList (){
        String text = "@TemazosDjFran tutu!!! olo lom. !!!qe??? pasas; !!!";

        List<String> words = new ArrayList<>();
        splitter.split(text, words);

        Assert.assertEquals(12, words.size());
        Assert.assertEquals("@TemazosDjFran".toUpperCase(), words.get(0));
        Assert.assertEquals("!!!", words.get(words.size()-1));

    }

    @Test
    public void testSplitToListShort (){
        String text = "tutu, t!!!";

        List<String> words = new ArrayList<>();
        splitter.split(text, words);

        Assert.assertEquals(4, words.size());
        Assert.assertEquals("tutu".toUpperCase(), words.get(0));
        Assert.assertEquals("!!!", words.get(words.size()-1));

    }

    @Test
    public void testSplitToListComplexPunctuator(){
        String text = "t!!! a";

        List<String> words = new ArrayList<>();
        splitter.split(text, words);

        Assert.assertEquals(3, words.size());
        Assert.assertEquals("t".toUpperCase(), words.get(0));
        Assert.assertEquals("a".toUpperCase(), words.get(words.size()-1));

    }
}
