package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.model.ChainTree;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.utils.TreeUtils;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by ifelix on 2/24/15.
 */
public class MarkovChainTreeBuilderTest {

    MarkovChainBuilder markovChainBuilder = new MarkovChainTreeBuilderImpl();
    Splitter splitter = new SplitterImpl();


    @Test
    public void testBuildTree() {
        String text = "tata, tutu!!! olo lom. !!!qe??? olo lom? tu tu tu. tata, tutu!!! olo lom?";

        List<String> words = new ArrayList<>();
        splitter.split(text, words);
        List<ChainTree> forest = markovChainBuilder.buildForest(words, 4);

        Assert.assertEquals(24, words.size());
        Assert.assertEquals(11, forest.size());

        ChainTree tree = TreeUtils.findByWord(forest, "tata");

        List<String> wordChain = new ArrayList<>();
        wordChain.add("tata");
        wordChain.add(",");
        wordChain.add("tutu");
        wordChain.add("!!!");
        checkChain(wordChain, tree, 2);

        tree = TreeUtils.findByWord(forest, "olo");
        wordChain.clear();
        wordChain.add("olo");
        wordChain.add("lom");
        checkChain(wordChain, tree, 3);

    }

    private void checkChain(List<String> words, ChainTree tree, int count) {
        Assert.assertTrue(words.size() <= TreeUtils.calculateDepth(tree));
        Iterator<String> wordsIterator = words.iterator();
        do {

            Assert.assertNotNull(tree);

            String word = wordsIterator.next();
            Assert.assertEquals(word.toUpperCase(), tree.getWord());
            Assert.assertEquals(count, tree.getCount());

            if (wordsIterator.hasNext()){
                Assert.assertTrue(2 > tree.getKids().size());
                tree = (tree.getKids().size() > 0) ? tree.getKids().get(0) : null;
            }
        } while ( wordsIterator.hasNext());
    }


}
