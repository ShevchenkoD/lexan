package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderSkipException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;


@Service(value = "documentProviderJSoupImpl")
public class DocumentProviderJSoupImpl implements DocumentProvider<Document> {
    public Document getDocumentFromUrl(String urlString) throws DocumentProviderException, DocumentProviderSkipException {
        Document result = null;

        int retryCount = 3;
        while (retryCount > 0) {

            try {

                Connection conn = Jsoup.connect(urlString);
                //conn.request().header("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
                conn
                        .userAgent("Mozilla")
                        .timeout(3000);

                conn.ignoreHttpErrors(true);
                result = conn.get();
                retryCount = 0;

            } catch (IOException e) {
                retryCount--;
                if (e instanceof SocketTimeoutException) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                        //log.err
                    }
                }
                if (e instanceof UnknownHostException) {
                    throw new DocumentProviderSkipException(e);
                }

                if (retryCount < 1) {
                    throw new DocumentProviderException(e);
                }
            }
        }

        return result;
    }
}
