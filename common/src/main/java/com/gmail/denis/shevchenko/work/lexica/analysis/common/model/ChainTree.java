package com.gmail.denis.shevchenko.work.lexica.analysis.common.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ChainTree {

    private String word;
    private int count = 1;
    private int kidsCount = 0;
    private ChainTree parent;
    private List<ChainTree> kids = new ArrayList<>();

    public ChainTree() {
    }

    public ChainTree(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<ChainTree> getKids() {
        return kids;
    }

    public void setKids(List<ChainTree> kids) {
        this.kids = kids;
    }

    public ChainTree getParent() {
        return parent;
    }

    public void setParent(ChainTree parent) {
        this.parent = parent;
    }

    public void add(List<String> items) {
        addToParent(this, items);
    }

    private void addToParent(ChainTree parent, List<String> items) {
        if (!items.isEmpty()) {
            Iterator<String> iterator = items.iterator();
            String first = iterator.next();

            ChainTree kidByWord = parent.getKidByWord(first);
            if (kidByWord == null) {
                kidByWord = new ChainTree(first);
                parent.getKids().add(kidByWord);
            } else {
                kidByWord.incCount();
            }
            parent.kidsCount++;

            addToParent(kidByWord, items.subList(1, items.size()));
        }
    }

    private void incCount() {
        count++;
    }


    public void add(ChainTree tree) {
        if (tree != null) {

            ChainTree kidByWord = getKidByWord(tree.getWord());
            if (kidByWord == null) {
                getKids().add(tree);
            } else {
                kidByWord.count += tree.count;
                addKids(tree.getKids(), kidByWord.getKids());
            }

        }
    }

    private void addKids(List<ChainTree> src, List<ChainTree> dst) {
        for (ChainTree item : src) {
            ChainTree kidByWord = getKidByWord(item.getWord());
            if (kidByWord == null) {
                dst.add(item);
            } else {
                kidByWord.count += item.count;
                kidByWord.add(item);
            }
        }

    }

    private ChainTree getKidByWord(String word) {
        ChainTree result = null;
        for (ChainTree item : kids) {
            if (item.getWord().equals(word)) {
                return item;
            }
        }

        return result;
    }
}
