package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text;

import java.util.List;
import java.util.Map;

public interface Splitter {
    void split(String body, Map<String, Integer> wordResult, Map<String, Integer> punctuatorResult);

    void split(String body, List<String> words);
}
