package com.gmail.denis.shevchenko.work.lexica.analysis.common.utils;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.model.ChainTree;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by ifelix on 2/24/15.
 */
public class TreeUtils {

    public static ChainTree findByWord(List<ChainTree> forest, String word) {
        word = word.toUpperCase();
        for (ChainTree tree :  forest){
            if (tree.getWord().equals(word)) {
                return tree;
            }
        }
        return null;
    }

    public static int calculateDepth(ChainTree tree) {
        int result = 1;
        if (tree == null) return 0;
        if ( tree.getKids().size() > 0 ){
            List<Integer> depthList = new ArrayList<>(tree.getKids().size());
            for (ChainTree kid : tree.getKids()) {
                depthList.add(calculateDepth(kid));
            }
            Collections.sort(depthList);
            result += depthList.get(0);
        }
        return result;
    }
}
