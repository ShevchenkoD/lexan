package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception;


public class DocumentProviderSkipException extends Exception {
    private static final long serialVersionUID = -4095830472113281755L;

    public DocumentProviderSkipException(Exception e) {
        super(e);
    }
}
