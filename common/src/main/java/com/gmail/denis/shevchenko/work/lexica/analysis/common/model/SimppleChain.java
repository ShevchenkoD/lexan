package com.gmail.denis.shevchenko.work.lexica.analysis.common.model;

import java.util.List;


public class SimppleChain {
    private String word;
    private int count;
    private SimppleChain next;

    public SimppleChain(List<String> items) {
        if (items.size() > 0) {
            this.word = items.get(0);
            this.count = 0;
        }

        if (items.size() > 1) {
            this.next = new SimppleChain(items.subList(1, items.size() - 1));
        }

    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public SimppleChain getNext() {
        return next;
    }

    public void setNext(SimppleChain next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return "ChainLink{" +
                "word='" + word + '\'' +
                ", count=" + count +
                ", next=" + next +
                '}';
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimppleChain)) return false;

        SimppleChain simppleChain = (SimppleChain) o;

        if (next != null ? !next.equals(simppleChain.next) : simppleChain.next != null) return false;
        if (!word.equals(simppleChain.word)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = word.hashCode();
        result = 31 * result + (next != null ? next.hashCode() : 0);
        return result;
    }
}
