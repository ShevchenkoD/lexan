package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception;

public class SkipPostException extends Exception {


    private static final long serialVersionUID = 6271202911833411072L;

    public SkipPostException(String message) {
        super(message);
    }

    public SkipPostException(String message, Throwable cause) {
        super(message, cause);
    }

    public SkipPostException(Throwable cause) {
        super(cause);
    }


}