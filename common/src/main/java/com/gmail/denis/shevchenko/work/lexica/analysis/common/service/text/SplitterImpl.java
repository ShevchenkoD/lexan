package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.gmail.denis.shevchenko.work.lexica.analysis.common.Constants.DELIMITERS;
import static com.gmail.denis.shevchenko.work.lexica.analysis.common.Constants.PUNCTUATORS;
import static com.gmail.denis.shevchenko.work.lexica.analysis.common.Constants.COMPLEX_PUNCTUATORS;

@Service
public class SplitterImpl implements Splitter {

    @Override
    public void split(String body, Map<String, Integer> wordResult, Map<String, Integer> punctuatorResult) {
        //body.split("\\W");
        //byte[] bytes = body.getBytes();
        char[] chars = body.toCharArray();

        List<Character> word = new ArrayList<>();
        List<Character> punctuator = new ArrayList<>();
        for (Character c : chars) {
            //new Character(c);
            if ((!DELIMITERS.contains(c)) && (!PUNCTUATORS.contains(c)) && (!COMPLEX_PUNCTUATORS.contains(c))) {
                if (punctuator.size() > 0) {
                    writeResultToMapAndClear(punctuatorResult, punctuator);
                }

                word.add(c);
            } else {
                if (punctuator.size() == 0) {
                    if (PUNCTUATORS.contains(c)) {
                        punctuator.add(c);
                        writeResultToMapAndClear(punctuatorResult, punctuator);
                    } else if (COMPLEX_PUNCTUATORS.contains(c)) {
                        punctuator.add(c);
                    }

                } else {

                    if ((!COMPLEX_PUNCTUATORS.contains(c)) || (!punctuator.get(0).equals(c))) {
                        writeResultToMapAndClear(punctuatorResult, punctuator);
                    }
                    punctuator.add(c);
                }

                if (word.size() > 0) {
                    writeResultToMapAndClear(wordResult, word);
                }
            }
        }
        if (word.size() > 0) {
            writeResultToMapAndClear(wordResult, word);
        }
        if (punctuator.size() > 0) {
            writeResultToMapAndClear(punctuatorResult, punctuator);
        }
    }

    @Override
    public void split(String body, List<String> words) {
        char[] chars = body.toCharArray();

        List<Character> word = new ArrayList<>();
        List<Character> punctuator = new ArrayList<>();
        for (Character c : chars) {
            //new Character(c);
            if ((!DELIMITERS.contains(c)) && (!PUNCTUATORS.contains(c)) && (!COMPLEX_PUNCTUATORS.contains(c))) {
                if (punctuator.size() > 0) {
                    writeResultToListAndClear(words, punctuator);
                }
                word.add(c);
            } else {

                if (word.size() > 0) {
                    writeResultToListAndClear(words, word);
                }

                if (punctuator.size() == 0) {
                    if (PUNCTUATORS.contains(c)) {
                        punctuator.add(c);
                        writeResultToListAndClear(words, punctuator);
                    } else if (COMPLEX_PUNCTUATORS.contains(c)) {
                        punctuator.add(c);
                    }

                } else {

                    if ((!COMPLEX_PUNCTUATORS.contains(c)) || (!punctuator.get(0).equals(c))) {
                        writeResultToListAndClear(words, punctuator);
                    }
                    if (!DELIMITERS.contains(c)){
                        punctuator.add(c);
                    }
                }
            }
        }
        if (word.size() > 0) {
            writeResultToListAndClear(words, word);
        }
        if (punctuator.size() > 0) {
            writeResultToListAndClear(words, punctuator);
        }
    }

    private void writeResultToMapAndClear(Map<String, Integer> result, List<Character> characters) {
        char[] cpunc = transform(characters);
        String cString = (new String(cpunc)).toUpperCase();

        if (result.get(cString) == null) {
            result.put(cString, 0);
        }
        result.put(cString, result.get(cString) + 1);

        characters.clear();
    }

    private void writeResultToListAndClear(List<String> result, List<Character> characters) {
        char[] cpunc = transform(characters);
        String cString = (new String(cpunc)).toUpperCase();
        result.add(cString);

        characters.clear();
    }

    private char[] transform(List<Character> word) {
        char[] cword = new char[word.size()];
        for (int i = 0; i < word.size(); i++) {
            cword[i] = word.get(i);
        }
        return cword;
    }

}
