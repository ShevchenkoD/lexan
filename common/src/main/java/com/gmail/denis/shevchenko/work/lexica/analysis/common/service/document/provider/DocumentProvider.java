package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider;


import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderSkipException;

public interface DocumentProvider<T> {
    public T getDocumentFromUrl(String urlString) throws DocumentProviderException, DocumentProviderSkipException;
}
