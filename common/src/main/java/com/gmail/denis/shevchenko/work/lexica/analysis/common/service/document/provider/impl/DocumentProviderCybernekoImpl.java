package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.cyberneko.html.parsers.DOMParser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


@Service(value = "cybernekoDocumentProvider")
public class DocumentProviderCybernekoImpl implements DocumentProvider<Document> {
    private static final Log log = LogFactory.getLog(DocumentProviderCybernekoImpl.class);

    @Override
    public Document getDocumentFromUrl(String urlString) throws DocumentProviderException {
        Document doc = null;
        try {
            URL url = new URL(urlString);

            URLConnection connection = url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            connection.connect();


            DOMParser parser = new DOMParser();
            //parser.se
            parser.parse(new InputSource(connection.getInputStream()));
            doc = parser.getDocument();

        /*DOMReader reader = new DOMReader();
        Document document = reader.read(doc);*/

        } catch (MalformedURLException e) {
            throw new DocumentProviderException(e);
        } catch (SAXException e) {
            throw new DocumentProviderException(e);
        } catch (IOException e) {
            throw new DocumentProviderException(e);
        }


        return doc;
    }
}
