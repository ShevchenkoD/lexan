package com.gmail.denis.shevchenko.work.lexica.analysis.common;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class Constants {


    public static final Charset UTF_8 = Charset.forName("UTF-8");

    public static List<Character> DELIMITERS = new ArrayList<>();

    static {
        DELIMITERS.add(' ');
        DELIMITERS.add('\t');
        DELIMITERS.add('\n');
    }

    public static List<Character> COMPLEX_PUNCTUATORS = new ArrayList<>();

    static {
        COMPLEX_PUNCTUATORS.add('.');
        COMPLEX_PUNCTUATORS.add('!');
        COMPLEX_PUNCTUATORS.add('¡');
        COMPLEX_PUNCTUATORS.add('?');
        COMPLEX_PUNCTUATORS.add('¿');
    }


    public static List<Character> PUNCTUATORS = new ArrayList<>();

    static {
        PUNCTUATORS.add(',');
        PUNCTUATORS.add(':');
        PUNCTUATORS.add(';');
        PUNCTUATORS.add('—');
        PUNCTUATORS.add('-');
        PUNCTUATORS.add('«');
        PUNCTUATORS.add('»');
        PUNCTUATORS.add('"');
        PUNCTUATORS.add('\'');
        PUNCTUATORS.add('(');
        PUNCTUATORS.add(')');
        PUNCTUATORS.add('[');
        PUNCTUATORS.add(']');
        PUNCTUATORS.add('{');
        PUNCTUATORS.add('}');
        PUNCTUATORS.add('*');
        PUNCTUATORS.add('_');
        PUNCTUATORS.add('+');
        PUNCTUATORS.add('/');
        PUNCTUATORS.add('\\');
        PUNCTUATORS.add('_');
        PUNCTUATORS.add('|');
        PUNCTUATORS.add('‘');
        PUNCTUATORS.add('’');
        PUNCTUATORS.add('‚');
        PUNCTUATORS.add('‛');
        PUNCTUATORS.add('“');
        PUNCTUATORS.add('”');
        PUNCTUATORS.add('„');
        PUNCTUATORS.add('‟');
        PUNCTUATORS.add('…');
        PUNCTUATORS.add('⁇');
        PUNCTUATORS.add('⁈');
        PUNCTUATORS.add('⁉');
        PUNCTUATORS.add('‼');
        PUNCTUATORS.add('‵');
        PUNCTUATORS.add('″');
        PUNCTUATORS.add('′');
        PUNCTUATORS.add('‐');
        PUNCTUATORS.add('‑');
        PUNCTUATORS.add('‒');
        PUNCTUATORS.add('–');
        PUNCTUATORS.add('—');
        PUNCTUATORS.add('―');
        PUNCTUATORS.add('‖');
    }

}
