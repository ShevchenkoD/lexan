package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.model.ChainTree;

import java.util.ArrayList;
import java.util.List;

public class MarkovChainTreeBuilderImpl implements MarkovChainBuilder {

    private static int minLength = 2;

    @Override
    public List<ChainTree> buildForest(List<String> words, int maxLength) {

        ChainTree tree = new ChainTree();
        for (int i = 0; i < words.size() - minLength + 1; i++) {
            int endIndex = i + maxLength;
            if (endIndex > words.size()) {
                endIndex = words.size();
            }
            tree.add(words.subList(i, endIndex));
        }

        return tree.getKids();
    }
}
