package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception;


public class DocumentProviderException extends Exception {
    private static final long serialVersionUID = -8482541890527909909L;

    public DocumentProviderException(Exception e) {
        super(e);
    }
}
