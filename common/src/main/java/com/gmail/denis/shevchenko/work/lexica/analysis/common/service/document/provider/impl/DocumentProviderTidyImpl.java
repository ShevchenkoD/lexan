package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.impl;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.document.provider.DocumentProvider;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.exception.DocumentProviderException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;


@Service(value = "documentProvider")
public class DocumentProviderTidyImpl implements DocumentProvider<Document> {
    private static final Log log = LogFactory.getLog(DocumentProviderTidyImpl.class);


    private Tidy tidy = new Tidy();


    public void init() {


    }

    @Override
    public Document getDocumentFromUrl(String urlString) throws DocumentProviderException {
        Document result;

        try {

            URL url = new URL(urlString);

            URLConnection connection = url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            connection.connect();

            PipedInputStream pis = new PipedInputStream();
            PipedOutputStream pos = new PipedOutputStream(pis);

            tidy.setXmlOut(true);
            tidy.setDocType("<!DOCTYPE html>");
            tidy.setWrapScriptlets(true);
            //org.w3c.tidy.Node tnode = tidy.parse(url.openStream(), pos);

    /*is = pis;

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    factory.setNamespaceAware(true); // never forget this!
    factory.setValidating(false);
    factory.setCoalescing(true);
    DocumentBuilder builder = factory.newDocumentBuilder();


    //is = url.openStream();
    result = builder.parse(is);*/
            try {
                result = tidy.parseDOM(connection.getInputStream(), pos);
            } catch (RuntimeException re) {
                log.error("post for string" + urlString, re);
                throw re;
            }
        } catch (MalformedURLException e) {
            throw new DocumentProviderException(e);
        } catch (IOException e) {
            throw new DocumentProviderException(e);
        }


        return result;
    }
}
