package com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.model.ChainTree;

import java.util.List;

public interface MarkovChainBuilder {
    List<ChainTree> buildForest(List<String> words, int maxLength);
}
