package com.gmail.denis.shevchenko.work.lexica.analysis.common.utils;

import java.nio.charset.Charset;


public class StringUtils {

    public static final Charset UTF_8_CHARSET = Charset.forName("UTF-8");
    public static final Charset ISO_8859_1_CHARSET = Charset.forName("ISO-8859-1");

}
