package com.gmail.denis.shevchenko.work.lexica.analysis.etl.word;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text.Splitter;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text.SplitterImpl;
import com.gmail.denis.shevchenko.work.lexica.analysis.etl.avro.gen.Post;
import org.apache.avro.Schema;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyInputFormat;
import org.apache.avro.mapreduce.AvroKeyValueOutputFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;


public class WordCountJob extends Configured implements Tool {
    static Log log = LogFactory.getLog(WordCountJob.class);

    static Splitter splitter = new SplitterImpl();

    public static void main(String... args) throws Exception {
        /*runJob(
                Arrays.copyOfRange(args, 0, args.length - 1),
                args[args.length - 1]);*/

        //        runJob(new String[]{"hdfs://localhost:9000/posts-avro/part-m-00000"}, "/result/temp/vocabulary.json");


        int res = ToolRunner.run(
                new WordCountJob(),
                new String[]{
                        "hdfs://localhost:9000/posts-avro/",
                        "hdfs://localhost:9000/out/word_count"}); //args
        System.exit(res);
    }


    public int run(String[] args) throws Exception {

        Job job = Job.getInstance(getConf());
        job.setJarByClass(WordCountJob.class);
        job.setJobName("Word Count");

        FileSystem fs = FileSystem.get(new URI("hdfs://localhost:9000"), getConf());

        FileInputFormat.setInputPaths(job, getPathArrayFromDir(args[0], fs));

        Path outDir = new Path(args[1]);
        fs.delete(outDir, true);
        FileOutputFormat.setOutputPath(job, outDir);

        job.setInputFormatClass(AvroKeyInputFormat.class);
        job.setMapperClass(WordMapper.class);
        AvroJob.setInputKeySchema(job, Post.getClassSchema());
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setOutputFormatClass(AvroKeyValueOutputFormat.class);
        job.setReducerClass(WordCountReducer.class);
        AvroJob.setOutputKeySchema(job, Schema.create(Schema.Type.STRING));
        AvroJob.setOutputValueSchema(job, Schema.create(Schema.Type.INT));

        return (job.waitForCompletion(true) ? 0 : 1);
    }

    private Path[] getPathArrayFromDir(String arg, FileSystem fs) throws IOException {
        List<Path> inputPathes = new ArrayList<Path>();
        Path inDir = new Path(arg);
        RemoteIterator<LocatedFileStatus> iterator = fs.listFiles(inDir, false);
        while (iterator.hasNext()) {
            LocatedFileStatus item = iterator.next();
            if (item.isFile() && (item.getLen() > 0)) {
                inputPathes.add(item.getPath());
            }
        }
        return inputPathes.toArray(new Path[inputPathes.size()]);
    }


    public static class WordMapper extends Mapper<AvroKey<Post>, NullWritable, Text, IntWritable> {

        @Override
        public void map(AvroKey<Post> key, NullWritable value, Context context)
                throws IOException, InterruptedException {

            CharSequence text = key.datum().getBodyUnaccent();
            if (text != null) {

                Map<String, Integer> wordResult = new HashMap<>();
                Map<String, Integer> punctuatorResult = new HashMap<>();

                splitter.split(text.toString(), wordResult, punctuatorResult);

                for (Map.Entry<String, Integer> item : wordResult.entrySet()) {
                    context.write(new Text(item.getKey()), new IntWritable(item.getValue()));
                }

                for (Map.Entry<String, Integer> item : punctuatorResult.entrySet()) {
                    context.write(new Text(item.getKey()), new IntWritable(item.getValue()));
                }

                log.debug(key.datum().getUrl());
            }

        }
    }


    public static class WordCountReducer extends
            Reducer<Text, IntWritable, AvroKey<CharSequence>, AvroValue<Integer>> {

        @Override
        public void reduce(Text key, Iterable<IntWritable> values,
                           Context context) throws IOException, InterruptedException {

            int sum = 0;
            for (IntWritable value : values) {
                sum += value.get();
            }
            context.write(new AvroKey<CharSequence>(key.toString()), new AvroValue<Integer>(sum));
        }
    }
}
