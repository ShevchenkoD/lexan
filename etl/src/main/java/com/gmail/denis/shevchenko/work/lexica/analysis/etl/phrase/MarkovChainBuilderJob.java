package com.gmail.denis.shevchenko.work.lexica.analysis.etl.phrase;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.model.ChainTree;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text.MarkovChainBuilder;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text.MarkovChainTreeBuilderImpl;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text.Splitter;
import com.gmail.denis.shevchenko.work.lexica.analysis.common.service.text.SplitterImpl;
import com.gmail.denis.shevchenko.work.lexica.analysis.etl.avro.AvroTools;
import com.gmail.denis.shevchenko.work.lexica.analysis.etl.avro.gen.Chain;
import com.gmail.denis.shevchenko.work.lexica.analysis.etl.avro.gen.Post;
import org.apache.avro.Schema;
import org.apache.avro.mapred.AvroKey;
import org.apache.avro.mapred.AvroValue;
import org.apache.avro.mapreduce.AvroJob;
import org.apache.avro.mapreduce.AvroKeyInputFormat;
import org.apache.avro.mapreduce.AvroKeyValueOutputFormat;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.ObjectWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;


public class MarkovChainBuilderJob extends Configured implements Tool {
    static Log log = LogFactory.getLog(MarkovChainBuilderJob.class);

    static Splitter splitter = new SplitterImpl();
    static MarkovChainBuilder chainBuilder = new MarkovChainTreeBuilderImpl();


    public static void main(String... args) throws Exception {
        /*runJob(
                Arrays.copyOfRange(args, 0, args.length - 1),
                args[args.length - 1]);*/

        //        runJob(new String[]{"hdfs://localhost:9000/posts-avro/part-m-00000"}, "/result/temp/vocabulary.json");


        int res = ToolRunner.run(
                new MarkovChainBuilderJob(),
                new String[]{
                        "hdfs://localhost:9000/posts-avro/",
                        "hdfs://localhost:9000/out/markov_chain"}); //args
        System.exit(res);
    }


    public int run(String[] args) throws Exception {

        Job job = Job.getInstance(getConf());
        job.setJarByClass(MarkovChainBuilderJob.class);
        job.setJobName("Markov Chain");

        FileSystem fs = FileSystem.get(new URI("hdfs://localhost:9000"), getConf());

        FileInputFormat.setInputPaths(job, getPathArrayFromDir(args[0], fs));

        Path outDir = new Path(args[1]);
        fs.delete(outDir, true);
        FileOutputFormat.setOutputPath(job, outDir);

        job.setInputFormatClass(AvroKeyInputFormat.class);
        job.setMapperClass(MarkovTreeMapper.class);
        AvroJob.setInputKeySchema(job, Post.getClassSchema());
        job.setOutputFormatClass(AvroKeyValueOutputFormat.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(ObjectWritable.class);

        job.setReducerClass(MarkovTreeReducer.class);
        AvroJob.setOutputKeySchema(job, Schema.create(Schema.Type.STRING));
        AvroJob.setOutputValueSchema(job, Chain.getClassSchema());

        return (job.waitForCompletion(true) ? 0 : 1);
    }

    private Path[] getPathArrayFromDir(String arg, FileSystem fs) throws IOException {
        List<Path> inputPathes = new ArrayList<Path>();
        Path inDir = new Path(arg);
        RemoteIterator<LocatedFileStatus> iterator = fs.listFiles(inDir, false);
        while (iterator.hasNext()) {
            LocatedFileStatus item = iterator.next();
            if (item.isFile() && (item.getLen() > 0)) {
                inputPathes.add(item.getPath());
            }
        }
        return inputPathes.toArray(new Path[inputPathes.size()]);
    }


    public static class MarkovTreeMapper extends Mapper<AvroKey<Post>, NullWritable, Text, ObjectWritable> {

        @Override
        public void map(AvroKey<Post> key, NullWritable value, Context context)
                throws IOException, InterruptedException {

            CharSequence text = key.datum().getBodyUnaccent();
            if ((text != null) && (text.length() > 0)) {
                List<String> words = new ArrayList<>();
                splitter.split(text.toString(), words);
                List<ChainTree> forest = chainBuilder.buildForest(words, 20);

                for (ChainTree item : forest) {
                    context.write(new Text(item.getWord()), new ObjectWritable(item));
                }

                log.debug(key.datum().getUrl());
            }

        }
    }


    public static class MarkovTreeReducer extends
            Reducer<Text, ObjectWritable, AvroKey<CharSequence>, AvroValue<Chain>> {

        @Override
        public void reduce(Text key, Iterable<ObjectWritable> values,
                           Context context) throws IOException, InterruptedException {

            ChainTree root = null;// = values.iterator() new ChainTree(key.toString());
            int count = 0;
            for (ObjectWritable value : values) {
                ChainTree tree = (ChainTree) value.get();
                if (root == null) {
                    root = tree;
                } else {
                    root.add(tree);
                }

                count++;
            }
            root.setCount(count);

            //TODO calculate frequencies
            // local frequency is ratio of sum of kid counts to kid count
            // global frequency is ratio of word count to count of all words
            //TODO normalize frequencies
            //delete chains with frequency lower than threshold
            //TODO TF-IDF

            context.write(new AvroKey<CharSequence>(key.toString()), new AvroValue<Chain>(AvroTools.transform(root)));

        }
    }
}
