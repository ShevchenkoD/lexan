package com.gmail.denis.shevchenko.work.lexica.analysis.etl.avro;

import com.gmail.denis.shevchenko.work.lexica.analysis.common.model.ChainTree;
import com.gmail.denis.shevchenko.work.lexica.analysis.etl.avro.gen.Chain;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ifelix on 2/23/15.
 */
public class AvroTools {

    public static Chain transform(ChainTree chainTree){
        Chain result = new Chain();

        result.setWord(chainTree.getWord());
        result.setCount(chainTree.getCount());
        result.setKids(transformForest(chainTree.getKids()));
        return result;
    }

    public static List<Chain> transformForest(List<ChainTree> forest){
        List<Chain> result = new ArrayList<Chain>();
        for (ChainTree tree: forest) {
             result.add(transform(tree));
        }
        return result;
    }

}
